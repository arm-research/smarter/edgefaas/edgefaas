variables:
  # When using dind service we need to instruct docker, to talk with the
  # daemon started inside of the service. The daemon is available with
  # a network connection instead of the default /var/run/docker.sock socket.
  #
  # The 'docker' hostname is the alias of the service container as described at
  # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
  #
  # Note that if you're using Kubernetes executor, the variable should be set to
  # tcp://localhost:2375 because of how Kubernetes executor connects services
  # to the job container
  DOCKER_HOST: tcp://docker:2375/
  # When using dind, it's wise to use the overlayfs driver for
  # improved performance.
  DOCKER_DRIVER: overlay2
  REGISTRY: registry.gitlab.com
  CONTAINER_IMAGE: $REGISTRY/$CI_PROJECT_PATH
  CONTAINER_FULL_IMAGE: $REGISTRY/$CI_PROJECT_PATH:${CI_COMMIT_REF_SLUG}
  CONTAINER_PART_IMAGE: $REGISTRY/$CI_PROJECT_PATH:${CI_COMMIT_REF_SLUG}-part
  CONTAINER_WDOG_IMAGE: $REGISTRY/$CI_PROJECT_PATH:${CI_COMMIT_REF_SLUG}-wdog
  CONTAINER_RELEASE_IMAGE: $REGISTRY/$CI_PROJECT_PATH:latest
  DOCKER_TLS_CERTDIR: ""

  # Multi-arch build support
  CI_BUILD_IMAGE: "registry.gitlab.com/ericvh/docker-buildx-qemu"
  CI_BUILDX_ARCHS: "linux/arm,linux/arm64,linux/amd64"

stages:
  - build
  - combine
  - release

partial-func:
  tags:
    - docker
  stage: build
  image: $CI_BUILD_IMAGE
  services:
  - name: docker:19.03.5-dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]
  retry: 2
  script:
    - update-binfmts --enable
    - docker buildx create --driver docker-container --use
    - docker buildx inspect --bootstrap
    - docker buildx ls
    - docker buildx build --platform $CI_BUILDX_ARCHS --progress plain --cache-from=$CONTAINER_PART_IMAGE -t $CONTAINER_PART_IMAGE -t $CONTAINER_IMAGE:${CI_COMMIT_SHA}-part . -f Dockerfile.func --push

watchdog:
  tags:
    - docker
  stage: build
  image: docker:stable
  services:
    - docker:19.03.5-dind
  artifacts:
    paths:
      - fwatchdog-arm
      - fwatchdog-arm64
      - fwatchdog-amd64
  script:
    - GIT_COMMIT=$CI_COMMIT_SHA
    - VERSION=$CI_COMMIT_REF_SLUG
    - docker build --progress plain --build-arg VERSION=$VERSION --build-arg GIT_COMMIT=$GIT_COMMIT --pull -t $CONTAINER_WDOG_IMAGE -t $CONTAINER_IMAGE:${CI_COMMIT_SHA}-wdog . -f Dockerfile.wdog
    - docker push $CONTAINER_IMAGE:${CI_COMMIT_SHA}-wdog
    - docker push $CONTAINER_WDOG_IMAGE
    - docker create --name builder $CONTAINER_WDOG_IMAGE echo
    - docker cp builder:/go/src/github.com/openfaas/faas/watchdog/watchdog-arm ./fwatchdog-arm
    - docker cp builder:/go/src/github.com/openfaas/faas/watchdog/watchdog-arm64 ./fwatchdog-arm64
    - docker cp builder:/go/src/github.com/openfaas/faas/watchdog/watchdog-amd64 ./fwatchdog-amd64
    - docker rm builder

full-func:master:
  only:
    - master
  tags:
    - docker
  stage: combine
  image: $CI_BUILD_IMAGE
  services:
  - name: docker:19.03.5-dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]
  retry: 2
  dependencies:
      - watchdog
  script:
    - update-binfmts --enable
    - docker buildx create --driver docker-container --use
    - docker buildx inspect --bootstrap
    - docker buildx ls
    - docker buildx build --platform $CI_BUILDX_ARCHS --progress plain --build-arg IMAGE_PART=$CONTAINER_PART_IMAGE --cache-from=$CONTAINER_FULL_IMAGE -t $CONTAINER_FULL_IMAGE -t $CONTAINER_RELEASE_IMAGE -t $CONTAINER_IMAGE:$CI_COMMIT_SHA . -f Dockerfile.full --push

full-func:non-master:
  except:
    - master
  tags:
    - docker
  stage: combine
  image: $CI_BUILD_IMAGE
  services:
  - name: docker:19.03.5-dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]
  retry: 2
  dependencies:
      - watchdog
  script:
    - update-binfmts --enable
    - docker buildx create --driver docker-container --use
    - docker buildx inspect --bootstrap
    - docker buildx ls
    - docker buildx build --platform $CI_BUILDX_ARCHS --progress plain --build-arg IMAGE_PART=$CONTAINER_PART_IMAGE --cache-from=$CONTAINER_FULL_IMAGE -t $CONTAINER_FULL_IMAGE -t $CONTAINER_IMAGE:$CI_COMMIT_SHA . -f Dockerfile.full --push

before_script:
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $REGISTRY
