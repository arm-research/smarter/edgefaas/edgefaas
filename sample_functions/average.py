# Copyright (c) Arm Ltd 2020
# Licensed under the MIT license. See LICENSE file in the project root for full license information.

def main():
    numbers="0,1,2,3,4,5,6,7,8,9,10"

    print "INPUT:"
    print numbers
    out = handle(numbers)
    
    print "\n\nOUTPUT:"
    print out

    return

def handle(req):

    try:
        numbers = req.split(",")
        qty = len(numbers)

        total = float(0)
        for idx in range(0,qty):
            total = total + float(numbers[idx])

        average = total/qty

        return average

    except Exception as e:
        return ('{\"Exception\": "' + str(e) + '"}')

if __name__== "__main__":
  main()

